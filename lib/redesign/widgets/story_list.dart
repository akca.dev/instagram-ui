import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:listapp/redesign/widgets/story_item.dart';

class StoryList extends StatefulWidget {
  final List<Color> gradientColors;
  final List<String> names;
  final List<String> imagesUrl;
  final double radius;

  const StoryList(
      {super.key,
      required this.gradientColors,
      required this.radius,
      required this.names, required this.imagesUrl});

  @override
  State<StoryList> createState() => _StoryListState();
}

class _StoryListState extends State<StoryList> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 80,
          child: ListView.builder(
              itemCount: widget.names.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: ((context, index) {
                widget.gradientColors.shuffle();
                return StoryItem(
                    name: widget.names[index],
                    liveIndex: index,
                    imageUrl: widget.imagesUrl[index],
                    gradientColors: widget.gradientColors,
                    radius: widget.radius);
              })),
        ),
        Divider(color: Colors.grey.withOpacity(.4)),
      ],
    );
  }
}
