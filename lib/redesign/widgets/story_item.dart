import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class StoryItem extends StatefulWidget {
  final liveIndex;
  final List<Color> gradientColors;
  final double radius;
  final String name;
  final String imageUrl;
  const StoryItem(
      {super.key, this.liveIndex, required this.gradientColors, required this.radius, required this.name, required this.imageUrl});

  @override
  State<StoryItem> createState() => _StoryItemState();
}


class _StoryItemState extends State<StoryItem> {
  @override
  Widget build(BuildContext context) {
    print(widget.liveIndex);
    return Container(
      margin: EdgeInsets.only(right: 5),
      child: Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                width: 60,
                height: 60,
                margin: EdgeInsets.only(right: 5, left: 5),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: widget.gradientColors,
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        stops: [0.2, 0.8, 1]),
                    borderRadius: BorderRadius.circular(widget.radius)),
              ),
              Container(
                width: 55,
                height: 55,
                margin: EdgeInsets.only(right: 5, left: 5),
                decoration: BoxDecoration(
                    color: Colors.grey,
                    image: DecorationImage(
                        image: NetworkImage("${widget.imageUrl}"),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(widget.radius)),
              ),
              widget.liveIndex == 2
                  ? Positioned(
                      bottom: 0,
                      child: Container(
                        width: 35,
                        height: 16,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            gradient: LinearGradient(
                                colors: [
                                  Color(0xffFBAA47),
                                  Color(0xffD91A46),
                                  Color(0xffA60F93),
                                ],
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                stops: [0.2, 0.5, 1]),
                            borderRadius: BorderRadius.circular(5)),
                        child: Text(
                          "LIVE",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "${widget.name}",
            style: TextStyle(fontSize: 12.5, letterSpacing: .2),
          )
        ],
      ),
    );
    ;
  }
}
