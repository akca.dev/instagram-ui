// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:math';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:listapp/redesign/widgets/story_list.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  CarouselController controller = CarouselController();
  List<Color> gradientColors = [
    Color(0xffFBAA47),
    Color(0xffD91A46),
    Color(0xffA60F93)
  ];

  int current = 0;

  List<String> names = ["Ramazan", "Imran", "Tarkan", "Emre", "Burak", "Bengi"];

  List<String> imagesUrl = [
    "https://sp-images.summitpost.org/947006.jpg?auto=format&fit=max&h=800&ixlib=php-2.1.1&q=35&s=876696700800816d01e0d1eb31ce7ab0",
    "https://i.pinimg.com/originals/ad/15/5b/ad155b4cfd5b6d220c3e5b51b349a37a.jpg",
    "https://i.pinimg.com/736x/00/ec/6b/00ec6b1a19a8dd9dee3949d4f7b09c1b.jpg",
    "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.venmond.com%2Fdemo%2Fvendroid%2Fimg%2Favatar%2Fbig.jpg&f=1&nofb=1",
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fseodesignchicago.com%2Fwp-content%2Fuploads%2F2020%2F12%2Fyoung-professional-SNP8S3N.jpg&f=1&nofb=1",
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.istockphoto.com%2Fphotos%2Fportrait-of-asian-chinese-businessman-picture-id614315942%3Fk%3D6%26m%3D614315942%26s%3D612x612%26w%3D0%26h%3DqYnuuWOnklapLY9Lu141j3BR_iKWjdI6v6D2XS6W460%3D&f=1&nofb=1"
  ];

  List<String> sliderUrl = [
    "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fmonde.ccdmd.qc.ca%2Fmedia%2Fimage1024%2F50418.jpg&f=1&nofb=1",
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.XObUMCVCTva9xJQUmhDQpgHaFj%26pid%3DApi&f=1",
    "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fmonde.ccdmd.qc.ca%2Fmedia%2Fimage1024%2F50418.jpg&f=1&nofb=1",
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.XObUMCVCTva9xJQUmhDQpgHaFj%26pid%3DApi&f=1",
  ];

  Random random = Random();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SvgPicture.asset("assets/logo.svg"),
        centerTitle: true,
        leading: IconButton(
            onPressed: () {}, icon: SvgPicture.asset("assets/camera.svg")),
        actions: [
          IconButton(
              onPressed: () {}, icon: SvgPicture.asset("assets/igtv.svg")),
          IconButton(
              onPressed: () {}, icon: SvgPicture.asset("assets/messanger.svg"))
        ],
        backgroundColor: Color(0xffFAFAFA),
        shadowColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Divider(color: Colors.blueGrey),
            StoryList(
                gradientColors: gradientColors,
                imagesUrl: imagesUrl,
                radius: 10,
                names: names),
            ListView.builder(
                itemCount: 2,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: ((context, index) {
                  return Container(
                    width: double.infinity,
                    height: 500,
                    margin: EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(color: Colors.grey),
                    child: Column(
                      children: [
                        Container(
                            width: double.infinity,
                            height: 55,
                            padding: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                            ),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        width: 40,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    "assets/profile.png")),
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      SizedBox(
                                        height: 35,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                  width: 1,
                                                ),
                                                Text(
                                                  "joshua_l",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                SizedBox(
                                                  width: 3,
                                                ),
                                                SvgPicture.asset(
                                                  "assets/tick.svg",
                                                  width: 12,
                                                )
                                              ],
                                            ),
                                            Text("Tokyo, Japan")
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  IconButton(
                                      onPressed: (() {}),
                                      icon: Icon(Icons.more_horiz))
                                ])),
                        Stack(
                          children: [
                            CarouselSlider(
                              carouselController: controller,
                              options: CarouselOptions(
                                  scrollDirection: Axis.vertical,
                                  autoPlay: true,
                                  autoPlayInterval: Duration(
                                      milliseconds: random.nextInt(4000)),
                                  onPageChanged: (index, reason) {
                                    print("$index");
                                    setState(() {
                                      current = index;
                                    });
                                  },
                                  height: 350.0,
                                  viewportFraction: 1),
                              items: sliderUrl.map((i) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return Container(
                                      width: double.infinity,
                                      height: 350,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: NetworkImage("$i"),
                                              fit: BoxFit.cover)),
                                    );
                                  },
                                );
                              }).toList(),
                            ),
                            Positioned(
                              top: 0,
                              bottom: 0,
                              left: 0,
                              child: GestureDetector(
                                onTap: () {
                                  controller.previousPage();
                                },
                                child: Container(
                                  child: Icon(Icons.arrow_left),
                                  decoration:
                                      BoxDecoration(color: Colors.white),
                                ),
                              ),
                            ),
                            Positioned(
                              top: 0,
                              bottom: 0,
                              right: 0,
                              child: GestureDetector(
                                onTap: () {},
                                child: Container(
                                  child: Icon(Icons.arrow_right),
                                  decoration:
                                      BoxDecoration(color: Colors.white),
                                ),
                              ),
                            )
                          ],
                        ),
                        Stack(
                          alignment: Alignment.center,
                          children: [
                            Container(
                              width: double.infinity,
                              height: 60,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      IconButton(
                                          onPressed: () {},
                                          icon: SvgPicture.asset(
                                              "assets/like.svg")),
                                      IconButton(
                                          onPressed: () {},
                                          icon: SvgPicture.asset(
                                              "assets/comment.svg")),
                                      IconButton(
                                          onPressed: () {},
                                          icon: SvgPicture.asset(
                                              "assets/messanger.svg"))
                                    ],
                                  ),
                                  IconButton(
                                      onPressed: () {},
                                      icon: SvgPicture.asset("assets/save.svg"))
                                ],
                              ),
                              decoration: BoxDecoration(color: Colors.white),
                            ),
                            Container(
                              height: 8,
                              child: ListView.builder(
                                  itemCount: sliderUrl.length,
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: ((context, index) {
                                    return AnimatedContainer(
                                        width: current == index ? 20 : 8,
                                        height: 8,
                                        margin: EdgeInsets.only(right: 3),
                                        decoration: BoxDecoration(
                                            color: current == index
                                                ? Colors.blue
                                                : Colors.grey.withOpacity(.6),
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                        duration: Duration(milliseconds: 280));
                                  })),
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                }))
          ],
        ),
      ),
    );
  }
}
