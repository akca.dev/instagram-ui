import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class ListScreen extends StatefulWidget {
  const ListScreen({super.key});

  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemCount: 8,
          itemBuilder: ((context, index) {
            print(index);
            if (index == 4) {
              return Container(
                height: 65,
                margin: EdgeInsets.only(bottom: 10),
                child: ListView.builder(scrollDirection: Axis.horizontal,itemCount: 10,itemBuilder:(context, index) {
                  return Container(
                    width: 30,
                    decoration: BoxDecoration(color: Colors.orange),
                    margin: EdgeInsets.only(right: 20)
                  );
                }, ),
                decoration: BoxDecoration(color: Colors.grey,
                borderRadius: BorderRadius.circular(20)),
              );
            } else {
              return Container(
                height: 65,
                margin: EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(color: Colors.deepOrange),
              );
            }
          })),
    );
  }
}
